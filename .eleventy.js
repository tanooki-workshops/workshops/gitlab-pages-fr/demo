
module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('./website/css')
  //eleventyConfig.addPassthroughCopy('js')
  //eleventyConfig.addPassthroughCopy('components')
  //eleventyConfig.addPassthroughCopy('pictures')

  return {
    passthroughFileCopy: true,
    dir: {
      input: "website",
      output: "public"
    }
  }
}